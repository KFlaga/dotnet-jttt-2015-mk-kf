﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnet_jttt
{
    // W klasie tworzone będą obiekty DataInput dla odpowiednich warunków
    class DataInputCreator
    {

        public DataInputCreator()
        {
            
        }

        public DataInput GetConditionInput(int num)
        {
            DataInput conditionInput = new DataInput();
            conditionInput.SetTitle(new string[] { "Jesli na stronie znajduje sie obrazek", "zawierajacy klucz w opisie" });
            conditionInput.AddTextInput("URL");
            return conditionInput;
        }

        public DataInput GetActionInput(int num)
        {
            DataInput actionInput = new DataInput();
            if (num == 0)
            {
                actionInput.SetTitle(new string[] { "Wyslij email z zalaczonym obrazkiem", "na podany adres email" });
                actionInput.AddTextInput("Email");
            }
            else
            {
                actionInput.SetTitle(new string[] { "Pokazuje obrazek w nowym oknie" });
            }
            return actionInput;
        }

    }
}
