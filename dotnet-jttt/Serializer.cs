﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;

namespace dotnet_jttt
{
    class Serializer
    {
        BinaryFormatter formatter;
        string fileName;

        public Serializer(string fn)
        {
            fileName = fn;
            formatter = new BinaryFormatter();
        }

        public void Serialize(BindingList<CondWithAction> obj)
        {
            FileStream file = new FileStream("fileName", FileMode.Create);

            try
            {
                formatter.Serialize(file, obj);
            }
            catch(SerializationException e)
            {
                MessageBox.Show("Niepowodzenie serializacji: " + e.Message);
            }
            finally
            {
                file.Close();
            }
        }

        public BindingList<CondWithAction> Deserialize()
        {
            FileStream file = new FileStream("fileName", FileMode.Open);
            BindingList<CondWithAction> list =  new BindingList<CondWithAction>();

            try
            {
                list = (BindingList<CondWithAction>)formatter.Deserialize(file);
            }
            catch(SerializationException e)
            {
                MessageBox.Show("Niepowodzenie serializacji: " + e.Message);
            }
            finally
            {
                file.Close();
            }
            return list;
        }

    }
}
