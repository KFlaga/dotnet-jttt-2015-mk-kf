﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.IO;
using System.Drawing;

namespace dotnet_jttt
{
    [Serializable]
    class DownloadImageIfConditionsMet: IObjectOnConditions
    {
        string htmlSource;
        Image image;
        string url;
        List<ICondition> condList;
        List<string> condParams = new List<string>();

        public DownloadImageIfConditionsMet()
        {
            condList = new List<ICondition>();
        }
            
        public void AddCondition(ICondition cond)
        {
            condList.Add(cond);
        }

        public void RemoveCondition(string label)
        {
            for (int i = 0; i < condList.Count; i++)
            {
                if (condList[i].Label == label)
                {
                    condList.RemoveAt(i);
                }
            }
        }

        private string GetPageHtml(string url)
        {
            // Pobiera źródło strony
            using (WebClient wc = new WebClient())
            {
                byte[] data = wc.DownloadData(url);
                string html = System.Net.WebUtility.HtmlDecode(Encoding.UTF8.GetString(data));

                return html;
            }
        }

        public void SetParams(string[] args)
        {
            url = args[0];
            if (args.Length != condList.Count + 1)
            {
                System.Windows.Forms.MessageBox.Show("Nieprawidłowa lista argumentów (bład aplikcji)");
                return;
            }
            for (int i = 0; i < condList.Count; i++)
            {
                condParams.Add(args[i + 1]);
            }
        }

        public void CheckCondition()
        {
            try
            {
                htmlSource = GetPageHtml(url);

                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlSource);

                var nodes = doc.DocumentNode.Descendants("img"); // W nodes mamy wszystkie węzły 'img' opisujące obrazki

                foreach (var node in nodes)
                {
                    bool conditionsMet = true;
                    for (int i = 0; i < condList.Count; i++)
                    {
                        condList[i].SetParams(node, condParams[i]);
                        if (!condList[i].Check())
                            conditionsMet = false;
                    }
                    if (conditionsMet == true)
                    {
                        if (node.GetAttributeValue("src", "").Contains("http:"))
                            DownloadImage(node.GetAttributeValue("src", ""));
                        else
                            DownloadImage((string)url + node.GetAttributeValue("src", ""));
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                image = null;
            }
        }

        private void DownloadImage(string src)
        {
            Logger.Instance.AddLog("Ściąganie obrazka");
            // Do image data ściągamy tablicę bitów, które powinny być obrazkiem
            // z podanego źródła
            byte[] imageData;
            try
            {
                using (WebClient wc = new WebClient())
                {
                    imageData = wc.DownloadData(src);
                }

                // Aby przerobić tablicę bitów na obrazek stosujemy MemoryStream
                MemoryStream ms = new MemoryStream(imageData);
                Image image = Image.FromStream(ms);

                this.image = image;
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show("Niepowodzenie pobrania obrazka z adresu: " + src + ". Szczegóły: " + e.Message);
            }
        }

        public object GetResult()
        {
            return image;
        }

        public override string ToString()
        {
            string condStrings = "";
            for (int i = 0; i < condList.Count; i++ )
            {
                condStrings += condList[i].ToString();
                condStrings += ": " + condParams[i] + ", ";
            }
            return "Jesli na stronie " + url + " jest obrazek spełniający warunki: " + condStrings;
        }
    }
}
