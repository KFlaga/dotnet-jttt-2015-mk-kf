﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dotnet_jttt
{
    class ConditionIfSmallerThan:ICondition
    {
        public string Label
        {
            get
            {
                return "Wysokość";
            }
        }

        HtmlAgilityPack.HtmlNode node;
        int size;

        public bool Check()
        {
            string accHeight = node.GetAttributeValue("height", "");
            if (accHeight == "") // obrazek nie ma podanej wysokości
                return false;
            if (int.Parse(accHeight) < size)
                return true;
            return false;
        }

        public void SetParams(object arg_node, object arg_size)
        {
            try
            {
                node = (HtmlAgilityPack.HtmlNode)arg_node;
                size = int.Parse((string)arg_size);
            }
            catch (Exception e)
            {
                MessageBox.Show("Podano złe parametry. Szczegóły: " + e.Message);
                throw e;
            }
        }

        public override string ToString()
        {
            return "Obrazek mniejszy niż podana wysokość";
        }
    }
}
