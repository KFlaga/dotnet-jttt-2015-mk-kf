﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dotnet_jttt
{
    [Serializable]
    class CondWithAction
    {
        public IObjectOnConditions Condition
        {
            get;
            set;
        }
        public IAction Action { get; set; }

        public CondWithAction()
        {

        }

        public override string ToString()
        {
            //jak jest opt to dodac
            return "Warunek: " + Condition.ToString() + ", akcja: " + Action.ToString();
        }
    }
}
