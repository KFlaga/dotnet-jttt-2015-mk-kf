﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;

namespace dotnet_jttt
{
    public partial class MainWindow : Form
    {
        BindingList<IObjectOnConditions> objects; // Lista obiektów, które mogą być zwracane przy spełnieniu warunków
        BindingList<IAction> actions; // Lista akcji do wyboru
        BindingList<ICondition> conditions; // Lista warunków do wyboru
        int curObj;
        int curAction;
        DataInputCreator diCreator; // Tworzy interfejsy do wpisywania dla poszczególnych warunków/akcji
        BindingList<CondWithAction> condsActions = new BindingList<CondWithAction>();
        Serializer serializer = new Serializer("serFile");
        CondWithAction currentCwa = new CondWithAction();

        public MainWindow()
        {
            InitializeComponent();
            diCreator = new DataInputCreator();

            InitConditionsAndActions();

            lbCondActions.DataSource = condsActions;

            Logger.Instance.AddLog("Inicjalizacja zakończona");
        }

        private void InitConditionsAndActions()
        {
            objects = new BindingList<IObjectOnConditions>();
            actions = new BindingList<IAction>();

            objects.Add(new DownloadImageIfConditionsMet());

            actions.Add(new EMailer());
            actions.Add(new ImageShower());

            combChooseAction.DataSource = actions;
            combChooseObject.DataSource = objects;

            conditions = new BindingList<ICondition>();
            conditions.Add(new ConditionIfSmallerThan());
            conditions.Add(new ConditionImageContainsKey());

            clbConditions.Items.Add(conditions[0]);
            clbConditions.Items.Add(conditions[1]);

            this.combChooseAction.SelectedIndex = 0;
            this.combChooseObject.SelectedIndex = 0;
        }

        private void combChooseObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Wraz ze zmiana indeksu comba zmienia sie numer warunku, a takze ustawiany jest nowy
            // interfejs dla danego warunku
            curObj = combChooseObject.SelectedIndex;

            mainLayout.Controls.Remove(diCondition);
            diCondition = diCreator.GetConditionInput(curObj);
            mainLayout.Controls.Add(diCondition, 1, 2);

            Logger.Instance.AddLog("Warunek zmieniony na: "+curObj.ToString());
            for(int i = 0; i < clbConditions.Items.Count; i++ ) // odczekować warunki na liście
            {
                clbConditions.SetItemChecked(i, false);
            } 
        }

        private void combChooseAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            curAction = combChooseAction.SelectedIndex;
            mainLayout.Controls.Remove(diAction);
            diAction = diCreator.GetActionInput(curAction);
            mainLayout.Controls.Add(diAction, 1, 5);
            Logger.Instance.AddLog("Akcja zmieniona na: " + curAction.ToString());
        }

        private void btnAddToList_Click(object sender, EventArgs e)
        {
            if (!diCondition.CheckIfAllFieldsAreFilled() || !diAction.CheckIfAllFieldsAreFilled())
            {
                MessageBox.Show("Należy wypełnić wszystkie pola");
                return;
            }

            CondWithAction cwa = new CondWithAction();
            cwa.Condition = objects[curObj];
            cwa.Condition.SetParams(diCondition.GetTextInput());
            cwa.Action = actions[curAction];
            cwa.Action.SetInput(diAction.GetTextInput());

            condsActions.Add(cwa);
            ResetLists();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            Logger.Instance.AddLog("Przycisk start wcisniety");
            // Sprawdzenie czy nie ma pustych miejsc

            for (int i = 0; i < condsActions.Count; i++)
            {
                condsActions[i].Condition.CheckCondition();
                object res = condsActions[i].Condition.GetResult();

                if (res == null)
                {
                    MessageBox.Show("Warunek " + i.ToString() + " nie spelniony\n");
                    continue;
                }

                condsActions[i].Action.SetCondResult(res);
                condsActions[i].Action.DoAction();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            condsActions.Clear();
        }

        private void btnSerialize_Click(object sender, EventArgs e)
        {
            serializer.Serialize(condsActions);
        }

        private void btnDeserialize_Click(object sender, EventArgs e)
        {
            condsActions = serializer.Deserialize();
            lbCondActions.DataSource = condsActions;
        }

        private void clbConditions_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.CurrentValue == CheckState.Unchecked)
            {
               diCondition.AddTextInput(conditions[e.Index].Label);
               objects[curObj].AddCondition(conditions[e.Index]);
            }
            else
            {
                diCondition.RemoveRow(conditions[e.Index].Label);
                objects[curObj].RemoveCondition(conditions[e.Index].Label);
            }
        }

        // Trochę nieładne resetowanie list, aby dodawać nowe warunki itd.
        // w przeciwnym wypadku byłyby dodawane te same obiekty lub trzeba by je klonować
        private void ResetLists()
        {
            conditions.Clear();
            clbConditions.Items.Clear();
            objects.Clear();
            actions.Clear();

            objects.Add(new DownloadImageIfConditionsMet());

            actions.Add(new EMailer());
            actions.Add(new ImageShower());

            combChooseObject.SelectedItem = 0;
            combChooseObject_SelectedIndexChanged(this, new EventArgs());
            combChooseAction.SelectedItem = 0;
            combChooseAction_SelectedIndexChanged(this, new EventArgs());

            conditions.Add(new ConditionIfSmallerThan());
            conditions.Add(new ConditionImageContainsKey());
  
            clbConditions.Items.Add(conditions[0]);
            clbConditions.Items.Add(conditions[1]);
        }

    }
}
