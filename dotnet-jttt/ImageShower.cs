﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dotnet_jttt
{
    [Serializable]
    class ImageShower : IAction
    {

        Image image;
        public ImageShower() { }

        public void SetCondResult(object img)
        {
            image = (Image)img;
        }

        public void SetInput(string[] input)
        {
            
        }

        // Tworzy nowe okienko z PictureBox'em w któym wyświetla się zadany obrazek
        public void DoAction()
        {
            Form picWindow = new Form();
            PictureBox picBox = new PictureBox();
            picWindow.AutoSize = true;
            picWindow.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            picBox.Size = image.Size;
            picBox.Image = image;

            picWindow.Controls.Add(picBox);
            picWindow.Show();
            Logger.Instance.AddLog("Wyświetlono obrazek");
        }

        public override string ToString()
        {
            return "Wyświetl obrazek";
        }
    }
}
