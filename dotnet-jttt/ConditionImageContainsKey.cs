﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dotnet_jttt
{
    class ConditionImageContainsKey:ICondition
    {
        public string Label
        {
            get
            {
                return "Klucz";
            }
        }
        
        HtmlAgilityPack.HtmlNode node;
        string key;

        public bool Check()
        {
            var cond = new ConditionContainSubstring();
            cond.SetParams(key, (node.GetAttributeValue("alt", "")));
            return cond.Check();
        }

        public void SetParams(object arg_node, object arg_key)
        {
            try
            {
                node = (HtmlAgilityPack.HtmlNode)arg_node;
                key = (string)arg_key;
            }
            catch (Exception e)
            {
                MessageBox.Show("Podano złe parametry. Szczegóły: " + e.Message);
                throw e;
            }
        }
        public override string ToString()
        {
            return "Opis obrazka zawiera podany klucz";
        }
    }
}
