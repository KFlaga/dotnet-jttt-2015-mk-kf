﻿namespace dotnet_jttt
{
    partial class MainWindow
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.labChooseCondition = new System.Windows.Forms.Label();
            this.labJesli = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labChooseAction = new System.Windows.Forms.Label();
            this.combChooseObject = new System.Windows.Forms.ComboBox();
            this.combChooseAction = new System.Windows.Forms.ComboBox();
            this.btnAddToList = new System.Windows.Forms.Button();
            this.lbCondActions = new System.Windows.Forms.ListBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSerialize = new System.Windows.Forms.Button();
            this.btnDeserialize = new System.Windows.Forms.Button();
            this.clbConditions = new System.Windows.Forms.CheckedListBox();
            this.labDostepneWarunki = new System.Windows.Forms.Label();
            this.labDodaneZadania = new System.Windows.Forms.Label();
            this.mainLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.AutoSize = true;
            this.mainLayout.ColumnCount = 2;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 410F));
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainLayout.Controls.Add(this.labChooseCondition, 1, 0);
            this.mainLayout.Controls.Add(this.labJesli, 0, 0);
            this.mainLayout.Controls.Add(this.label4, 0, 3);
            this.mainLayout.Controls.Add(this.labChooseAction, 1, 3);
            this.mainLayout.Controls.Add(this.combChooseObject, 1, 1);
            this.mainLayout.Controls.Add(this.combChooseAction, 1, 4);
            this.mainLayout.Location = new System.Drawing.Point(12, 12);
            this.mainLayout.MinimumSize = new System.Drawing.Size(500, 405);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 6;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainLayout.Size = new System.Drawing.Size(510, 405);
            this.mainLayout.TabIndex = 13;
            // 
            // labChooseCondition
            // 
            this.labChooseCondition.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labChooseCondition.AutoSize = true;
            this.labChooseCondition.Location = new System.Drawing.Point(229, 7);
            this.labChooseCondition.Name = "labChooseCondition";
            this.labChooseCondition.Size = new System.Drawing.Size(151, 13);
            this.labChooseCondition.TabIndex = 3;
            this.labChooseCondition.Text = "Wybierz obiekt zainteresowań:";
            // 
            // labJesli
            // 
            this.labJesli.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labJesli.AutoSize = true;
            this.labJesli.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labJesli.Location = new System.Drawing.Point(9, 18);
            this.labJesli.Name = "labJesli";
            this.mainLayout.SetRowSpan(this.labJesli, 2);
            this.labJesli.Size = new System.Drawing.Size(81, 24);
            this.labJesli.TabIndex = 4;
            this.labJesli.Text = "Jesli to:";
            this.labJesli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(18, 220);
            this.label4.Name = "label4";
            this.mainLayout.SetRowSpan(this.label4, 2);
            this.label4.Size = new System.Drawing.Size(64, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "To to:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labChooseAction
            // 
            this.labChooseAction.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labChooseAction.AutoSize = true;
            this.labChooseAction.Location = new System.Drawing.Point(266, 209);
            this.labChooseAction.Name = "labChooseAction";
            this.labChooseAction.Size = new System.Drawing.Size(77, 13);
            this.labChooseAction.TabIndex = 6;
            this.labChooseAction.Text = "Wybierz akcję:";
            // 
            // combChooseObject
            // 
            this.combChooseObject.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.combChooseObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combChooseObject.FormattingEnabled = true;
            this.combChooseObject.Location = new System.Drawing.Point(109, 23);
            this.combChooseObject.Name = "combChooseObject";
            this.combChooseObject.Size = new System.Drawing.Size(391, 21);
            this.combChooseObject.TabIndex = 0;
            this.combChooseObject.SelectedIndexChanged += new System.EventHandler(this.combChooseObject_SelectedIndexChanged);
            // 
            // combChooseAction
            // 
            this.combChooseAction.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.combChooseAction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combChooseAction.FormattingEnabled = true;
            this.combChooseAction.Items.AddRange(new object[] {
            "Wyślij obrazek na email",
            "Pokaz obrazek na ekranie"});
            this.combChooseAction.Location = new System.Drawing.Point(109, 225);
            this.combChooseAction.Name = "combChooseAction";
            this.combChooseAction.Size = new System.Drawing.Size(391, 21);
            this.combChooseAction.TabIndex = 7;
            this.combChooseAction.SelectedIndexChanged += new System.EventHandler(this.combChooseAction_SelectedIndexChanged);
            // 
            // btnAddToList
            // 
            this.btnAddToList.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnAddToList.Location = new System.Drawing.Point(528, 307);
            this.btnAddToList.Name = "btnAddToList";
            this.btnAddToList.Size = new System.Drawing.Size(68, 103);
            this.btnAddToList.TabIndex = 8;
            this.btnAddToList.Text = "Dodaj do listy";
            this.btnAddToList.UseVisualStyleBackColor = true;
            this.btnAddToList.Click += new System.EventHandler(this.btnAddToList_Click);
            // 
            // lbCondActions
            // 
            this.lbCondActions.FormattingEnabled = true;
            this.lbCondActions.HorizontalScrollbar = true;
            this.lbCondActions.Location = new System.Drawing.Point(602, 207);
            this.lbCondActions.Name = "lbCondActions";
            this.lbCondActions.Size = new System.Drawing.Size(370, 147);
            this.lbCondActions.TabIndex = 14;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(602, 362);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(114, 48);
            this.btnStart.TabIndex = 15;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(724, 362);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 48);
            this.btnClear.TabIndex = 16;
            this.btnClear.Text = "Czyść";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSerialize
            // 
            this.btnSerialize.Location = new System.Drawing.Point(833, 360);
            this.btnSerialize.Name = "btnSerialize";
            this.btnSerialize.Size = new System.Drawing.Size(139, 21);
            this.btnSerialize.TabIndex = 17;
            this.btnSerialize.Text = "Serializuj";
            this.btnSerialize.UseVisualStyleBackColor = true;
            this.btnSerialize.Click += new System.EventHandler(this.btnSerialize_Click);
            // 
            // btnDeserialize
            // 
            this.btnDeserialize.Location = new System.Drawing.Point(833, 387);
            this.btnDeserialize.Name = "btnDeserialize";
            this.btnDeserialize.Size = new System.Drawing.Size(139, 23);
            this.btnDeserialize.TabIndex = 18;
            this.btnDeserialize.Text = "Deserializuj";
            this.btnDeserialize.UseVisualStyleBackColor = true;
            this.btnDeserialize.Click += new System.EventHandler(this.btnDeserialize_Click);
            // 
            // clbConditions
            // 
            this.clbConditions.FormattingEnabled = true;
            this.clbConditions.Location = new System.Drawing.Point(602, 46);
            this.clbConditions.Name = "clbConditions";
            this.clbConditions.Size = new System.Drawing.Size(370, 124);
            this.clbConditions.TabIndex = 19;
            this.clbConditions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbConditions_ItemCheck);
            // 
            // labDostepneWarunki
            // 
            this.labDostepneWarunki.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labDostepneWarunki.AutoSize = true;
            this.labDostepneWarunki.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labDostepneWarunki.Location = new System.Drawing.Point(598, 23);
            this.labDostepneWarunki.Name = "labDostepneWarunki";
            this.labDostepneWarunki.Size = new System.Drawing.Size(204, 20);
            this.labDostepneWarunki.TabIndex = 20;
            this.labDostepneWarunki.Text = "Lista dostępnych warunków";
            this.labDostepneWarunki.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labDodaneZadania
            // 
            this.labDodaneZadania.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labDodaneZadania.AutoSize = true;
            this.labDodaneZadania.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labDodaneZadania.Location = new System.Drawing.Point(598, 184);
            this.labDodaneZadania.Name = "labDodaneZadania";
            this.labDodaneZadania.Size = new System.Drawing.Size(164, 20);
            this.labDodaneZadania.TabIndex = 21;
            this.labDodaneZadania.Text = "Lista dodanych zadań";
            this.labDodaneZadania.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 441);
            this.Controls.Add(this.labDodaneZadania);
            this.Controls.Add(this.labDostepneWarunki);
            this.Controls.Add(this.clbConditions);
            this.Controls.Add(this.btnDeserialize);
            this.Controls.Add(this.btnSerialize);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnAddToList);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lbCondActions);
            this.Controls.Add(this.mainLayout);
            this.MinimumSize = new System.Drawing.Size(1000, 480);
            this.Name = "MainWindow";
            this.Text = "JTTT";
            this.mainLayout.ResumeLayout(false);
            this.mainLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.Label labChooseCondition;
        private System.Windows.Forms.Label labJesli;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labChooseAction;
        private System.Windows.Forms.ComboBox combChooseObject;
        private System.Windows.Forms.ComboBox combChooseAction;
        private System.Windows.Forms.Button btnAddToList;
        private DataInput diCondition;
        private DataInput diAction;
        private System.Windows.Forms.ListBox lbCondActions;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSerialize;
        private System.Windows.Forms.Button btnDeserialize;
        private System.Windows.Forms.CheckedListBox clbConditions;
        private System.Windows.Forms.Label labDostepneWarunki;
        private System.Windows.Forms.Label labDodaneZadania;
    }
}

